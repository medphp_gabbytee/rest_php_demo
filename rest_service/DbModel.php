<?php
/**
* DbModel.php
*
* @author Gabriel Trujillo C. <joaqt23@gmail.com>
* @copyright Copyright (c) 2012
* @version 2.0.1
* @abstract Clase principal para modelo de interacción con bases de datos
* 
*/


if (!defined('DEBUG_MODE')) define('DEBUG_MODE', FALSE);

error_reporting((DEBUG_MODE?1:0));

/**
* DbModel
* 
* Clase principal para Modelo de interacción con base de datos
*
* @author Gabriel Trujillo C. <joaqt23@gmail.com>
* @copyright Copyright (c) 2012
* @version 1.2.1
*/

abstract class DbModel {

    #Definición propiedades

    /**#@+
    * @internal 
    * @var string
    * Variables de conexión al motor de bases de datos
    */        
    private static $_hostname = 'localhost';
    private static $_username = 'root';
    private static $_password = '8453dedatos';
    private static $_port = '3306';
    /**#@-*/

    /**
    * @var string $dbname: Nombre de la base de datos
    * 
    * Permite definir la base de datos sobre la que se realizarán las consultas
    */        
    protected $dbname = 'medphp';

    /**#@+
    * @var int
    * Constantes de apertura o cierre de conexiones
    */
    const CLOSE_CONNECTION = 0;
    const OPEN_CONNECTION = 1;

    /**#@+
    * @var int
    * Constantes de tipo de resultado a retornar
    */
    const RESULT_TYPE_DEFAULT = 0;
    const RESULT_TYPE_ARRAY_BOTH = 1;
    const RESULT_TYPE_ARRAY_NUM = 2;
    const RESULT_TYPE_ARRAY_ASSOC = 3;
    const RESULT_TYPE_ARRAY_COLUMN = 4;
    const RESULT_TYPE_OBJECT = 5;
    const RESULT_TYPE_JSON = 6;

    /**
    * get_type($var): Función interna para verificar el tipo de variable enviada.
    *
    * Este método recibe la variable $var y determina su tipo, bien sea 
    * - 's': texto
    * - 'i': entero 
    * - 'd': punto flotante
    * 
    * para establecer el tipo de delimitación requerido.
    *
    * @param mixed $var La variable recibida.
    * @return String
    */

    protected function get_type($var='')
    {
        if(is_string($var)) 
                return 's'; 
        else if(is_int($var)) 
                return 'i'; 
        else if(is_float($var)) 
                return 'd'; 
        else return 's'; 
    }

    /**
    * get_dump(&$var): Función interna para recibir el volcado de una variable.
    *
    * Este método recibe la variable $var por referencia y muestra el volcado de su contenido
    * empleando la instrucción var_dump para retornarlo como una cadena concatenable.
    *
    * @param mixed $var La variable recibida.
    * @return String
    * @throws Exception
    */

    protected function get_dump(&$var)
    {
        try {
            ob_start();
            var_dump($var);
            $dump = ob_get_clean();
            return $dump;
        }
        catch (Exception $exc) {
            throw new Exception('Excepción de datos.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.') '.$exc->getMessage() : ''));
        }
    }

    /**
    * connMySQL($option=self::OPEN,&$conn2close=NULL): Conexión MySQL típica procedimental.
    *
    * Este método realiza una nueva conexión procedimental y la retorna, o
    * finaliza la conexión definida en el segundo parámetro opcional
    * y retorna un valor lógico que determina si se pudo o no efectuar la desconexión.
    *
    * @param int $option self::OPEN_CONNECTION para crear una nueva conexión (por defecto) o
    *   self::CLOSE_CONNECTION para cerrar la conexión dada por $conn2close.
    * @param mixed $conn2close Conexión MySQL a cerrar, pasada por referencia.
    * @return mixed
    * @throws Exception
    */

    protected final function connMySQL($option=self::OPEN_CONNECTION,&$conn2close=NULL) {	// <--- Conexión MySQL típica procedimental
        if ($option===self::OPEN_CONNECTION) {
            $conn = mysql_connect(self::$_hostname,self::$_username,self::$_password);
            if (!$conn) throw new Exception('Error: No fue posible conectarse con la base de datos.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".mysql_error() : ''));
            mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $conn);
            return $conn;
        } else if ($option===self::CLOSE_CONNECTION) {
            mysql_close($conn2close);
            return TRUE;
        } else {
            return FALSE;
        }
    }	

    /**
    * connMySQLiObj($option=self::OPEN_CONNECTION,mysqli &$conn2close=NULL): Conexión MySQLi por objetos.
    *
    * Este método realiza una nueva conexión MySQLi por objetos y la retorna, o
    * finaliza la conexión definida en el segundo parámetro opcional pasado por referencia
    * y retorna un valor lógico que determina si se pudo o no efectuar la desconexión.
    *
    * @param int $option self::OPEN_CONNECTION para crear una nueva conexión (por defecto) o
    *   self::CLOSE para cerrar la conexión dada por $conn2close.
    * @param mysqli $conn2close Conexión a cerrar, pasada por referencia.
    * @return mysqli
    * @throws Exception
    */

    protected final function connMySQLiObj($option=self::OPEN_CONNECTION,mysqli &$conn2close=NULL) { // <--- Conexión MySQLi por objetos
        if ($option===self::OPEN_CONNECTION) {
            $conn = new mysqli(self::$_hostname,self::$_username,self::$_password,$this->dbname);
            if ($conn->connect_errno) throw new Exception('Error: No fue posible conectarse con la base de datos.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$conn->connect_error : ''));
            return $conn;
        } else if ($option===self::CLOSE_CONNECTION) {
            $conn2close->close();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    * connPDO($option=self::OPEN_CONNECTION,PDO &$conn2close=NULL): Conexión PDO (únicamente por objetos).
    *
    * Este método realiza una nueva conexión PDO y la retorna, o finaliza la 
    * conexión definida en el segundo parámetro opcional y retorna un valor
    * lógico que determina si se pudo o no efectuar la desconexión.
    *
    * @param int $option self::OPEN_CONNECTION: para crear una nueva conexión (por defecto) o
    *   self::CLOSE_CONNECTION para cerrar la conexión dada por $conn2close.
    *
    * @param PDO $conn2close Conexión a cerrar por referencia.
    * @return PDO
    * @throws Exception
    * 
    * @todo Verificar disfuncionalidad de PDO::MYSQL_ATTR_READ_DEFAULT_FILE => '/etc/my.cnf' en Windows 7
    */	

    protected final function connPDO($option=self::OPEN_CONNECTION,PDO &$conn2close=NULL) {	// <--- Conexión PDO por objetos
        if ($option===self::OPEN_CONNECTION) {
            try {
                $pdo = new PDO(
                    'mysql:host='.self::$_hostname.';port='.self::$_port.';dbname='.$this->dbname,self::$_username,self::$_password,
                    array(
                        //PDO::MYSQL_ATTR_READ_DEFAULT_FILE => '/etc/my.cnf',
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')
                    ); 
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $pdo;
            }
            catch(PDOException $ex) {
                throw new Exception('Error: No fue posible conectarse con la base de datos.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$ex->getMessage() : ''));
            }
        } else if ($option===self::CLOSE_CONNECTION) {
            $conn2close=NULL;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    * runSQL($sql='',&$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0): Consulta MySQL procedimental.
    *
    * Este método realiza de forma procedimental la consulta 
    * determinada por la variable $sql y retorna el resultset  
    * obtenido o 'false' si no se pudo completar.
    * Permite enviarle una conexión por referencia en el segundo argumento para realizar la 
    * consulta por medio de ella.
    * En el tercer argumento determina el tipo de conjunto de resultados a retornar, así:
    * - <<class>>::RESULT_TYPE_ARRAY_NUM: Matriz por índices numéricos.
    * - <<class>>::RESULT_TYPE_ARRAY_ASSOC: Matriz asociativa.
    * - <<class>>::RESULT_TYPE_ARRAY_BOTH: Matriz con índices numéricos y claves asociativas.
    * - <<class>>::RESULT_TYPE_ARRAY_COLUMN: Devuelve la columna dada por el argumento $index del
    *     conjunto de resultados.
    * - <<class>>::RESULT_TYPE_OBJECT: Colección de objetos stdClass cuyas propiedades corresponden
    *     con los nombres de campo recibidos.
    * - <<class>>::RESULT_TYPE_DEFAULT: Retorna un conjunto típico de resultados MySQL (opción por defecto).
    *
    * @param string $sql Texto de la consulta a ejecutar.
    * @param mixed $conn Conexión MySQL por referencia.
    * @param int $toType determina el tipo de conjunto de resultados a retornar.
    * @param int $index Si el parámetro anterior es <<class>>::RESULT_TYPE_ARRAY_COLUMN, establece el índice de
     *  base 0 de la columna a retornar (por defecto 0 -la primera columna-).
    * @return mixed
    * @throws Exception
    */

    protected final function runSQL($sql='',&$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0) {
        try {
            $toArray = (
                $toType === self::RESULT_TYPE_ARRAY_NUM ||
                $toType === self::RESULT_TYPE_ARRAY_ASSOC ||
                $toType === self::RESULT_TYPE_ARRAY_BOTH
            );
            $arrayType = 0;
            if ($toArray) {
                switch ($toType) {
                    case self::RESULT_TYPE_ARRAY_NUM:
                        $arrayType = MYSQL_NUM;
                        break;
                    case self::RESULT_TYPE_ARRAY_ASSOC:
                        $arrayType = MYSQL_ASSOC;
                        break;
                    case self::RESULT_TYPE_ARRAY_BOTH:
                        $arrayType = MYSQL_BOTH;
                        break;
                    default:
                        $arrayType = MYSQL_BOTH;
                        break;
                }
            }
            if (empty($conn)) {
                $conn = $this->connMySQL(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            mysql_select_db($this->dbname);
            $result = mysql_query($sql);
            if (!$result) throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".mysql_error()." | SQL: $sql | toType: $toType | index: $index" : ''));
            if (!$with_conn) $this->connMySQL(self::CLOSE_CONNECTION,$conn);
            if ($toArray) {
                $arr2ret = array();
                while ($row = mysql_fetch_array($result,$arrayType)) {
                    $arr2ret[] = $row;
                }
                return $arr2ret;
            }
            else if ($toType===self::RESULT_TYPE_OBJECT) {
                $arr2ret = array();
                while ($obj_row = mysql_fetch_object($result)) {
                    $arr2ret[] = $obj_row;
                }
                return $arr2ret;
            }
            else if ($toType===self::RESULT_TYPE_ARRAY_COLUMN) {
                $arr2ret = array();
                while ($row = mysql_fetch_array($result,MYSQL_NUM)) {
                    $arr2ret[] = $row[$index];
                }
                return $arr2ret;
            }
            else return $result;
        }
        catch (Exception $exc) {
            throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$exc->getMessage()." | SQL: $sql | toType: $toType | index: $index" : ''));
        }
    }

    /**
    * runSelectMysqli($sql,mysqli &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0): Consulta mysqli
    *   directa por objetos.
    *
    * Este método realiza la consulta directa (sin preparación) determinada por 
    * la variable $sql y retorna el resultset obtenido, o 'false' si no se pudo completar.
    * Permite enviarle una conexión mysqli por referencia en el segundo argumento para realizar la
    * consulta por medio de ella.
    * En el tercer argumento determina el tipo de conjunto de resultados a retornar, así:
    * - <<class>>::RESULT_TYPE_ARRAY_NUM: Matriz por índices numéricos.
    * - <<class>>::RESULT_TYPE_ARRAY_ASSOC: Matriz asociativa.
    * - <<class>>::RESULT_TYPE_ARRAY_BOTH: Matriz con índices numéricos y claves asociativas.
    * - <<class>>::RESULT_TYPE_ARRAY_COLUMN: Devuelve la columna dada por el argumento $index del
    *     conjunto de resultados.
    * - <<class>>::RESULT_TYPE_OBJECT: Colección de objetos stdClass cuyas propiedades corresponden
    *     con los nombres de campo recibidos.
    * - <<class>>::RESULT_TYPE_DEFAULT: Retorna un conjunto típico de resultados mysqli (opción por defecto).
    *
    * Este método es recomendable si se va a hacer un llamado a funciones o procedimientos
    * almacenados que no reciban parámetros o que no sean susceptibles a inyección SQL.
    *
    * @param string $sql Texto de la consulta a ejecutar.
    * @param mysqli $conn Conexión mysqli por referencia.
    * @param int $toType determina el tipo de conjunto de resultados a retornar.
    * @param int $index Si el parámetro anterior es <<class>>::RESULT_TYPE_ARRAY_COLUMN, establece el índice de
     *  base 0 de la columna a retornar (por defecto 0 -la primera columna-).
    * @return mixed 
    * @throws Exception
    */

    protected final function runSelectMysqli($sql,mysqli &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0) {
        try {
            $toArray = (
                $toType === self::RESULT_TYPE_ARRAY_NUM ||
                $toType === self::RESULT_TYPE_ARRAY_ASSOC ||
                $toType === self::RESULT_TYPE_ARRAY_BOTH
            );
            $arrayType = 0;
            if ($toArray) {
                switch ($toType) {
                    case self::RESULT_TYPE_ARRAY_NUM:
                        $arrayType = MYSQLI_NUM;
                        break;
                    case self::RESULT_TYPE_ARRAY_ASSOC:
                        $arrayType = MYSQLI_ASSOC;
                        break;
                    case self::RESULT_TYPE_ARRAY_BOTH:
                        $arrayType = MYSQLI_BOTH;
                        break;
                    default:
                        $arrayType = MYSQLI_BOTH;
                        break;
                }
            }
            if (empty($conn)) {
                $conn = $this->connMySQLiObj(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $result = $conn->query($sql);
            if (!$result) throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$conn->connect_error." | SQL:$sql" : ''));
            if (!$with_conn) $this->connMySQLiObj(self::CLOSE_CONNECTION,$conn);
            if ($toArray) {
                $arr2ret = array();
                while ($row = $result->fetch_array($arrayType)) {
                    $arr2ret[] = $row;
                }
                return $arr2ret;
            }
            else if ($toType===self::RESULT_TYPE_OBJECT) {
                $arr2ret = array();
                while ($obj_row = $result->fetch_object()) {
                    $arr2ret[] = $obj_row;
                }
                return $arr2ret;
            }
            else if ($toType===self::RESULT_TYPE_ARRAY_COLUMN) {
                $arr2ret = array();
                while ($row = $result->fetch_array(MYSQLI_NUM)) {
                    $arr2ret[] = $row[$index];
                }
                return $arr2ret;
            }
            else return $result;
        }
        catch (Exception $exc) {
            throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$exc->getMessage()." | SQL: $sql | arr_val: {$this->get_dump($arr_val)} | toType: $toType | index: $index" : ''));
        }
    }

    /**
    * runSelectSQLiPrepObj($sql,array $arr_val,mysqli &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0):
    *   Consulta mysqli preparada por objetos.
    *
    * Este método realiza la consulta preparada determinada por la variable $sql usando la librería mysqli
    * de php y retorna el resultset obtenido, o 'false' si no se pudo completar.
    *
    * Se debe suministrar una matriz asociativa con los valores a reemplazar en la consulta preparada
    * en su orden respectivo y, si es necesario, se envía en el tercer argumento el nombre de la conexión
    * por referencia para usarla.
    * En el cuarto argumento determina el tipo de conjunto de resultados a retornar, así:
    * - <<class>>::RESULT_TYPE_ARRAY_NUM: Matriz por índices numéricos.
    * - <<class>>::RESULT_TYPE_ARRAY_ASSOC: Matriz asociativa.
    * - <<class>>::RESULT_TYPE_ARRAY_BOTH: Matriz con índices numéricos y claves asociativas.
    * - <<class>>::RESULT_TYPE_ARRAY_COLUMN: Devuelve la columna dada por el argumento $index del
    *     conjunto de resultados.
    * - <<class>>::RESULT_TYPE_OBJECT: Colección de objetos stdClass cuyas propiedades corresponden
    *     con los nombres de campo recibidos.
    * - <<class>>::RESULT_TYPE_DEFAULT: Retorna un conjunto típico de resultados mysqli (opción por defecto).
    *
    * @param string $sql Texto de la consulta de inserción a ejecutar.
    * @param array $arr_val Matriz de valores a reemplazar.
    * @param mysqli $conn Conexión mysqli por objetos, pasada por referencia.
    * @param int $toType determina el tipo de conjunto de resultados a retornar.
    * @param int $index Si el parámetro anterior es <<class>>::RESULT_TYPE_ARRAY_COLUMN, establece el índice de
     *  base 0 de la columna a retornar (por defecto 0 -la primera columna-).
    * @return mixed 
    * @throws Exception
    */

    protected final function runSelectSQLiPrepObj($sql,array $arr_val,mysqli &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0) {
        try {
            $toArray = (
                $toType === self::RESULT_TYPE_ARRAY_NUM ||
                $toType === self::RESULT_TYPE_ARRAY_ASSOC ||
                $toType === self::RESULT_TYPE_ARRAY_BOTH
            );
            $arrayType = 0;
            if ($toArray) {
                switch ($toType) {
                    case self::RESULT_TYPE_ARRAY_NUM:
                        $arrayType = MYSQLI_NUM;
                        break;
                    case self::RESULT_TYPE_ARRAY_ASSOC:
                        $arrayType = MYSQLI_ASSOC;
                        break;
                    case self::RESULT_TYPE_ARRAY_BOTH:
                        $arrayType = MYSQLI_BOTH;
                        break;
                    default:
                        $arrayType = MYSQLI_BOTH;
                        break;
                }
            }
            if (empty($conn)) {
                $conn = $this->connMySQLiObj(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $stmt = $conn->prepare($sql);
            if (!$stmt) {
                throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$stmt->error." | SQL: $sql | arr_val: {$this->get_dump($arr_val)} | toType: $toType | index: $index" : ''));
            }
            if (count($arr_val) > 0) {
                $command = "\$stmt->bind_param('";
                $types = '';
                $vars = '';
                $nvar = 0;
                foreach ($arr_val as $key=>$value) {
                    $types .= $this->get_type($value);
                    $vars .= "\$arr_val[$key],";
                    $nvar++;
                }
                $command .= $types."',".substr($vars,0,-1).');';
    //		print_r($arr_val);
    //		die($sql.'<br>'.$command);
                eval('return '.$command);
                $stmt->execute();
                if ($stmt->error) {
                    throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$stmt->error." | SQL: $sql | $command | arr_val: {$this->get_dump($arr_val)} | toType: $toType | index: $index" : ''));
                }
                $result = $stmt->get_result();

                if (!$with_conn) $this->connMySQLiObj(self::CLOSE_CONNECTION,$conn);
                if ($toArray) {
                    $arr2ret = array();
                    while ($row = $result->fetch_array($arrayType)) {
                        $arr2ret[] = $row;
                    }
                    $stmt->close();
                    return $arr2ret;
                }
                else if ($toType===self::RESULT_TYPE_OBJECT) {
                    $arr2ret = array();
                    while ($obj_row = $result->fetch_object()) {
                        $arr2ret[] = $obj_row;
                    }
                    $stmt->close();
                    return $arr2ret;
                }
                else if ($toType===self::RESULT_TYPE_ARRAY_COLUMN) {
                    $arr2ret = array();
                    while ($row = $result->fetch_array(MYSQLI_NUM)) {
                        $arr2ret[] = $row[$index];
                    }
                    $stmt->close();
                    return $arr2ret;
                }
                else {
                    $stmt->close();
                    return $result;
                }
            }
            else return FALSE;
        }
        catch (Exception $exc) {
            throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$exc->getMessage()." | SQL: $sql | {$this->get_dump($arr_val)} | toType: $toType | index: $index" : ''));
        }
    }

    /**
    * execNonSelectSQLiObj($sql,array $arr_val,mysqli &$conn=NULL): Consulta no SELECT MySQLi preparada
    *   con argumentos.
    *
    * Este método realiza la consulta de no selección (inserción, 
    * eliminación, actualización, etc.) determinada por la variable $sql
    * usando la librería mysqli de php y retorna el número de filas 
    * afectadas por la instrucción, o 'false' si no fue exitosa.
    *
    * Cuando se desea realizar una consulta de inserción, se suministra
    * una matriz con los valores a insertar/modificar en la variable $arr_val, y
    * si es necesario se envía el nombre de la conexión por referencia para usarla.
    *
    * @param string $sql Texto de la consulta de inserción a ejecutar.
    * @param array $arr_val Matriz de valores a insertar (si se realizará una consulta de inserción
    *   o modificación).
    * @param mysqli $conn Conexión mysqli por objetos, pasada por referencia.
    * @return mixed 
    * @throws Exception
    */		

    protected final function execNonSelectSQLiObj($sql,array $arr_val,mysqli &$conn=NULL) {
        try {
            if (empty($conn)) {
                $conn = $this->connMySQLiObj(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $stmt = $conn->prepare($sql);
            if (!$stmt) {
                throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$stmt->error." | SQL: $sql | arr_val: {$this->get_dump($arr_val)}" : ''));
            }
            if (count($arr_val) > 0) {
                $command = "\$stmt->bind_param('";
                $types = '';
                $vars = '';
                $nvar = 0;
                foreach ($arr_val as $key=>$value) {
                    $types .= $this->get_type($value);
                    $vars .= "\$arr_val[$key],";
                    $nvar++;
                }
                $command .= $types."',".substr($vars,0,-1).');';
    //		print_r($arr_val);
    //		die($sql.'<br>'.$command);
                eval('return '.$command);
                $stmt->execute();
                if ($stmt->error) {
                    throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$stmt->error." | SQL: $sql | $command | {$this->get_dump($arr_val)}" : ''));
                }
                $num_rows = $stmt->affected_rows;
                $stmt->close();
            }
            else $num_rows = FALSE;
            if (!$with_conn) $this->connMySQLiObj(self::CLOSE_CONNECTION,$conn);
            return $num_rows;
        }
        catch (Exception $exc) {
            throw new Exception('Error: No se realizó la consulta. '.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$exc->getMessage()." | SQL: $sql | arr_val: {$this->get_dump($arr_val)}" : ''));
        }
    }

    /**
    * runSelectPDO($sql,PDO &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0): Consulta PDO por objetos.
    *
    * Este método realiza empleando la librería PDO la consulta determinada por 
    * la variable $sql y retorna el resultset obtenido o 'false' si no se pudo 
    * completar.
    * Permite enviarle una conexión PDO por referencia en el segundo argumento para realizar
    * la consulta por medio de ella.
    * En el tercer argumento determina el tipo de conjunto de resultados a retornar, así:
    * - <<class>>::RESULT_TYPE_ARRAY_NUM: Matriz por índices numéricos.
    * - <<class>>::RESULT_TYPE_ARRAY_ASSOC: Matriz asociativa.
    * - <<class>>::RESULT_TYPE_ARRAY_BOTH: Matriz con índices numéricos y claves asociativas.
    * - <<class>>::RESULT_TYPE_ARRAY_COLUMN: Devuelve la columna dada por el argumento $index del
    *     conjunto de resultados.
    * - <<class>>::RESULT_TYPE_OBJECT: Colección de objetos stdClass cuyas propiedades corresponden
    *     con los nombres de campo recibidos.
    * - <<class>>::RESULT_TYPE_DEFAULT: Retorna un objeto PDOStatement (opción por defecto).
    *
    * @param string $sql Texto de la consulta a ejecutar.
    * @param PDO $conn Conexión PDO por referencia.
    * @param int $toType Tipo de resultado a retornar (self::RESULT_TYPE_DEFAULT por defecto).
    * @param int $index Si el parámetro anterior es <<class>>::RESULT_TYPE_ARRAY_COLUMN, establece el
    *   índice base 0 de la columna a retornar (por defecto 0 -la primera columna-).
    * @return mixed
    * @throws Exception
    */

    protected final function runSelectPDO($sql,PDO &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0) {
        try {
            if (empty($conn)) {
                $conn = $this->connPDO(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $stmt = $conn->query($sql);
            if (!$with_conn) $this->connPDO(self::CLOSE_CONNECTION,$conn);
            if (!$stmt) return FALSE;
            else {
                switch ($toType) {
                    case self::RESULT_TYPE_ARRAY_BOTH:
                        return $stmt->fetchAll(PDO::FETCH_BOTH);
                        break;
                    case self::RESULT_TYPE_ARRAY_NUM:
                        return $stmt->fetchAll(PDO::FETCH_NUM);
                        break;
                    case self::RESULT_TYPE_ARRAY_ASSOC:
                        return $stmt->fetchAll(PDO::FETCH_ASSOC);
                        break;
                    case self::RESULT_TYPE_OBJECT:
                        return $stmt->fetchAll(PDO::FETCH_OBJ);
                        break;
                    case self::RESULT_TYPE_ARRAY_COLUMN:
                        return $stmt->fetchAll(PDO::FETCH_COLUMN,$index);
                        break;
                    default:
                        return $stmt;
                        break;
                }
            }
        }
        catch (Exception $ex) {
            throw new Exception('Error: No se realizó la consulta.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$ex->getMessage()." | SQL: $sql | toType: $toType | index: $index" : ''));
        }
    }

    /**
    * runSelectPDOPrep($sql,array $arr_val,PDO &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0):
    *   Consulta PDO preparada.
    *
    * Este método realiza la consulta preparada determinada por la variable $sql usando la librería
    * PDO de php y retorna el resultset obtenido, o 'false' si no se no se pudo completar la tarea.
    *
    * Se debe suministrar una matriz asociativa con los valores a reemplazar en la consulta preparada
    * y, si es necesario, se envía en el tercer argumento el nombre de la conexión por referencia.
    * En el cuarto argumento puede definirse el tipo de objeto a retornar usando las constantes de la clase PDO:
    * - <<class>>::RESULT_TYPE_ARRAY_NUM: Matriz por índices numéricos.
    * - <<class>>::RESULT_TYPE_ARRAY_ASSOC: Matriz asociativa.
    * - <<class>>::RESULT_TYPE_ARRAY_BOTH: Matriz con índices numéricos y claves asociativas.
    * - <<class>>::RESULT_TYPE_ARRAY_COLUMN: Devuelve la columna dada por el argumento $index del
    *     conjunto de resultados.
    * - <<class>>::RESULT_TYPE_OBJECT: Colección de objetos stdClass cuyas propiedades corresponden
    *     con los nombres de campo recibidos.
    * - <<class>>::RESULT_TYPE_DEFAULT: Retorna un objeto PDOStatement (opción por defecto).
    *
    * Este método es recomendable si se va a hacer un llamado a funciones o procedimientos
    * almacenados y/o sentencias susceptibles a inyección SQL.
    *
    * @param string $sql Texto de la consulta preparada a ejecutar.
    * @param array $arr_val Matriz de valores a insertar (si se realizará una consulta de inserción o
    *    modificación).
    * @param PDO $conn Conexión PDO por referencia.
    * @param int $toType Tipo de resultado a retornar (self::RESULT_TYPE_DEFAULT por defecto).
    * @param int $index Si el parámetro anterior es <<class>>::RESULT_TYPE_ARRAY_COLUMN, establece el índice
    *   base 0 de la columna a retornar (por defecto la primera columna, o 0).
    * @return mixed 
    * @throws Exception
    */

    protected final function runSelectPDOPrep($sql,array $arr_val,PDO &$conn=NULL,$toType=self::RESULT_TYPE_DEFAULT,$index=0) {
        try {
            if (empty($conn)) {
                $conn = $this->connPDO(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);
            $stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $stmt->execute($arr_val);
            if (!$with_conn) $this->connPDO(self::CLOSE_CONNECTION,$conn);
            if ($result) {
                switch ($toType) {
                    case self::RESULT_TYPE_ARRAY_BOTH:
                        return $stmt->fetchAll(PDO::FETCH_BOTH);
                        break;
                    case self::RESULT_TYPE_ARRAY_NUM:
                        return $stmt->fetchAll(PDO::FETCH_NUM);
                        break;
                    case self::RESULT_TYPE_ARRAY_ASSOC:
                        return $stmt->fetchAll(PDO::FETCH_ASSOC);
                        break;
                    case self::RESULT_TYPE_OBJECT:
                        return $stmt->fetchAll(PDO::FETCH_OBJ);
                        break;
                    case self::RESULT_TYPE_ARRAY_COLUMN:
                        return $stmt->fetchAll(PDO::FETCH_COLUMN,$index);
                        break;
                    default:
                        return $stmt;
                        break;
                }
            }
            else return FALSE;
        }
        catch (Exception $ex) {
            throw new Exception('Error: No se realizó la consulta.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$ex->getMessage()." | SQL: $sql | arr_val: {$this->get_dump($arr_val)} | toType: $toType | index: $index" : ''));
        }
    }

    /**
    * execNonSelectPDO($sql,array $arr_val,PDO &$conn): Consulta no SELECT PDO preparada con argumentos.
    *
    * Este método realiza la consulta de no selección (inserción, eliminación, actualización, etc.)
    * determinada por la variable $sql usando la librería PDO de php y retorna el número de filas
    * afectadas por la instrucción.
    *
    * Cuando se desea realizar una consulta de inserción, se suministra una matriz con los valores a
    * insertar/modificar en la variable $arr_val, y si es necesario se envía el nombre de la conexión PDO
    * por referencia.
    *
    * @param string $sql Texto de la consulta de aqcción a ejecutar.
    * @param array $arr_val Matriz asociativa de valores a insertar (si se realizará una consulta de inserción
    *   o modificación).
    * @param PDO $conn Conexión PDO por referencia. 
    * @return mixed 
    * @throws Exception
    */			

    protected final function execNonSelectPDOPrep($sql,array $arr_val,PDO &$conn=NULL) {
        try {
            if (empty($conn)) {
                $conn = $this->connPDO(self::OPEN_CONNECTION);
                $with_conn = FALSE;
            }
            else $with_conn = TRUE;
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);
            $stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->execute($arr_val);
            $num_rows = $stmt->rowCount();
            if (!$with_conn) $this->connPDO(self::CLOSE_CONNECTION,$conn);
            return $num_rows;
        }
        catch (Exception $ex) {
            throw new Exception('Error: No se realizó la consulta.'.(DEBUG_MODE ? "\nVolcado: (Llamado desde ".__METHOD__.") ".$ex->getMessage()." | SQL: $sql | arr_val: {$this->get_dump($arr_val)}" : ''));
        }
    }
}

