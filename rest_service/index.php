<?php

require_once('ModelUsuarios.php');

class BasicRestController {

    private $method = "";
    private $resources = array();
    private $data = "";
    private $response = array();
    private $resultCodeHeader = "";
    private $resultContentType = "";

    public function __construct() {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->resources = explode('/',$_SERVER['PATH_INFO']);
    }

    protected function setResultCodeHeader($code) {
        $http_status_codes = array(100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 300 => "Multiple Choices", 301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed", 406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm", 422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests", 431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls", 451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert", 497 => "HTTP to HTTPS", 499 => "Client Closed Request", 500 => "Internal Server Error", 501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported", 506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded", 510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error");
        $this->resultCodeHeader = "HTTP/1.1 $code {$http_status_codes[$code]}";
    }

    public function processResponse() {
        try {
            $usuarios = new ModelUsuarios();
            $result = null;
            switch ($this->method) {
                case 'get':
                    $this->data = $_GET;
                    if (array_key_exists(2,$this->resources)) {
                        $result = $usuarios->getUsuario($this->resources[2])->fetchAll(PDO::FETCH_ASSOC);
                        $resultCode = (count($result) > 0 ? 200 : 204);
                        $this->setResultCodeHeader($resultCode);
                    }
                    else {
                        $result = $usuarios->getAllUsuarios()->fetchAll(PDO::FETCH_ASSOC);
                        $resultCode = (count($result) > 0 ? 200 : 204);
                        $this->setResultCodeHeader($resultCode);
                    }
                    break;
                case 'post':
                    $this->data = $_POST;
                    if (array_key_exists('email',$this->data) && array_key_exists('pass',$this->data)) {
                        $usuario = new ModelUsuarios();
                        $usuario->email = $this->data['email'];
                        $usuario->pass = $this->data['pass'];
                        $result = $usuarios->postUsuario($usuario)->fetchAll(PDO::FETCH_ASSOC);
                        $this->setResultCodeHeader(201);
                    }
                    else {
                        $this->setResultCodeHeader(400);
                    }
                    break;
                case 'put':
                    parse_str(file_get_contents('php://input'), $put_vars);
                    $this->data = $put_vars;
                    if (array_key_exists(2,$this->resources) && array_key_exists('email',$this->data) && array_key_exists('pass',$this->data)) {
                        $usuario = new ModelUsuarios();
                        $usuario->id = $this->resources[2];
                        $usuario->email = $this->data['email'];
                        $usuario->pass = $this->data['pass'];
                        $result = $usuarios->putUsuario($usuario)->fetchAll(PDO::FETCH_ASSOC);
                        $this->setResultCodeHeader(200);
                    }
                    else {
                        $this->setResultCodeHeader(400);
                    }
                    break;
                case 'delete':
                    parse_str(file_get_contents('php://input'), $put_vars);
                    $this->data = $put_vars;
                    if (array_key_exists(2,$this->resources)) {
                        $result = $usuarios->deleteUsuario($this->resources[2]);
                        $this->setResultCodeHeader(200);
                    }
                    else {
                        $this->setResultCodeHeader(400);
                    }
                    break;
                default:
                    $this->setResultCodeHeader(405);
                    break;
            }
            $this->response['message'] = 'OK';
            $this->response['data'] = $result;
        }
        catch (Exception $exc) {
            $this->setResultCodeHeader(400);
            $this->response['message'] = $exc->getMessage();
        }
        return;
    }

    public function getResponse() {
        header($this->resultCodeHeader);
        header('Content-type: application/json');
        //header($this->resultContentType);
        echo(json_encode($this->response));

    }
}

$basicRestController = new BasicRestController();
$basicRestController->processResponse();
$basicRestController->getResponse();
