<?php

require_once('DbModel.php');

class ModelUsuarios extends DbModel {
    public $id;
    public $email;
    public $pass;
    public $result;

    public function getAllUsuarios() {
        try {
            $this->result = $this->runSelectPDO(
                "CALL get_all_usuarios()"
            );
            return $this->result;
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function getUsuario($id = 0) {
        try {
            $this->result = $this->runSelectPDOPrep(
                "CALL get_usuario(:id)",
                array('id'=>$id)
            );
            return $this->result;
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function postUsuario(ModelUsuarios $usuario) {
        try {
            $this->result = $this->runSelectPDOPrep(
                "CALL post_usuario(:id,:email,:pass)",
                array(
                    'id'=>0,
                    'email'=>$usuario->email,
                    'pass'=>$usuario->pass
                )
            );
            return $this->result;
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function putUsuario(ModelUsuarios $usuario) {
        try {
            $this->result = $this->runSelectPDOPrep(
                "CALL put_usuario(:id,:email,:pass)",
                array(
                    'id'=>$usuario->id,
                    'email'=>$usuario->email,
                    'pass'=>$usuario->pass
                )
            );
            return $this->result;
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function deleteUsuario($id) {
        try {
            $this->result = $this->execNonSelectPDOPrep(
                "CALL delete_usuario(:id)",
                array('id'=>$id)
            );
            return $this->result;
        }
        catch (Exception $exc) {
            return array();
        }
    }

} 