<?php

class RestConsumer {

	private $_method = "";
	private $_url = "";
	private $_data = array();
	private $_result;
	
	public function __construct($method, $url, $data = array()) {
		$this->_method = $method;
		$this->_url = $url;
		$this->_data = $data;
	}

	public function process() {
		try {
			$curl = curl_init();
			switch ($this->_method)
			{
				case "POST":
					curl_setopt($curl, CURLOPT_POST, 1);
					if (count($this->_data) > 0) {
                        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->_data));
                    }
                    break;
				case "PUT":
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->_data));
					break;
				case "DELETE":
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");	
					break;
				default:
					if ($this->_data)
						$this->_url = sprintf("%s?%s", $this->_url, http_build_query($this->_data));
			}
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "medphp_gabbytee:fielding6");
			curl_setopt($curl, CURLOPT_URL, $this->_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$this->_result = curl_exec($curl);
            $this->_result = ($this->_result == false ? curl_error($curl) : $this->_result);
            curl_close($curl);
		}
		catch (Exception $exc) {
			$this->_result = false;
		}
	}
	
	public function getResult() {
		return $this->_result;
	}

}