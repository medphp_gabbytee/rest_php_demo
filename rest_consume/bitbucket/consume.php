<?php
require_once('../RestConsumer.php');
$url = "https://bitbucket.org/api/1.0/repositories/medphp_gabbytee/rest_php_demo/issues";
$method = 'GET';
$data = array (
    'title' => '',
    'content' => ''
);
$data = null;
$restConsumer = new RestConsumer($method, $url, $data);
$restConsumer->process();
$response = $restConsumer->getResult();

echo "<pre>$response</pre>";